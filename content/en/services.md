---
layout: "services"
title: "Services"
translationKey: "services"
subtitle: Here you can get
cards:
  - title: "A project"
    link: "A short description to rescue the project"
    linkContent: "alink/toyourproject"
    icon: "ambulance"
  - title: "A service"
    icon: "bicycle"
    mpNum: "Some text"
  - title: "Social media"
    user: "juggleliasrod"
    link: "https://somerandomsite.com"
    linkContent: "@ProfileLink"
    icon: "address-card"
---

