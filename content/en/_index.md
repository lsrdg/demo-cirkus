---
layout: 'langhome'
slug: '/en/'
title: 'home'
services:
  - name: 'A responsive hugo theme'
    text: 'Bulma Css Framework and Sass support packaged in.'
  - name: 'Multilingual'
    text: "Support to Hugo's multiligual mode is a top priority."
  - name: 'Main goals'
    text: 'Mobile first, usuable despite of a bad network connection, responsiveness, multiligual support.'
---
