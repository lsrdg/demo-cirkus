---
layout: "services"
title: "Servoj"
translationKey: "services"
subtitle: Ĉi tite vi povas havi
cards:
  - title: "Projekto"
    link: "Mallonga priskribado"
    linkContent: "ligilo/alviaprojekto"
    icon: "ambulance"
  - title: "Servo"
    icon: "bicycle"
    mpNum: "Kaj iuj ajn teksto ekzemple"
  - title: "Sociaj komunikiloj"
    user: "Uzantnomo"
    link: "https://iujajnadreso.com"
    linkContent: "@Profilligilo"
    icon: "address-card"
---

