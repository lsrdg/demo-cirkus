---
layout: "langhome"
slug: "/eo/"
title: "home"
services:
  - name: "Esperanto"
    text: "Circus shows for all ages and humors, rooted on the streets."
  - name: "Teaching"
    text: "Workshops and classes for all levels on juggling, unicycle, clown, street theater, balance and comic stunts etc"
  - name: "Languages"
    text: "Shows and workshops in English, Danish, Esperanto, Portuguese, Spanish and in silence as well!"
---
