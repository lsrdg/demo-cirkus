module gitlab.com/lsrdg/demo-cirkus

// For local development, assuming the theme dir is at the same level as the site dir:
// uncomment the line below:
replace gitlab.com/lsrdg/cirkus => ../cirkus

go 1.16

require gitlab.com/lsrdg/cirkus v0.0.0-20210604111906-45814defa4d7 // indirect
